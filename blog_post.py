# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields
from openerp.addons.website.models.website import slug
from openerp.tools.translate import _
import base64
from PIL import Image
import datetime
from openerp import SUPERUSER_ID
from transliterate import translit


class blog_post(osv.Model):
    _name = "blog.post"
    _inherit = ['blog.post']
    _columns = {
        'name_rus': fields.char('Name (rus)'),
        'show': fields.boolean('Show in Frontpage'),
        'background_image': fields.binary('Background Image'),
        'background_image_small': fields.char('Background Image Small'),
        'social_image': fields.char('Social Image'),
        'comment': fields.integer('Comment'),
        'last_time_comment': fields.datetime('Last Comment'),
        'time_filter': fields.char('Time Filter'),
        'question_id': fields.many2one('question', 'Question'),
        'ask': fields.boolean('Ask Category'),
        'website': fields.selection([('aditim', 'Aditim'),
                                     ('polymeres', 'Polymeres'),
                                     ], 'Website'),
    }
    _defaults = {
        'two_column': False,
        'comment': 0,
    }

    def write(self, cr, uid, ids, vals, context=None):

        if vals.get('show'):
            blog_post_ids = self.search(
                cr, uid, [('show', '=', True)], context=context)
            self.write(cr, uid, blog_post_ids, {'show': False}, context=context)

        if vals.get('background_image'):
            imgdata = base64.b64decode(vals.get('background_image'))
            file_name = str(ids[0]) + '_blog_post'
            # f = open('openerp/addons/website_aditim/static/images/blog_post_images/%s.jpg' % file_name, 'wb')
            f = open('/home/odoo/website_aditim/static/images/blog_post_images/%s.jpg' % file_name, 'wb')
            f.write(imgdata)
            f.close()
            # f = Image.open('/home/odoo/website_aditim/static/images/blog_post_images/%s.jpg' % file_name)
            # # f = Image.open('openerp/addons/website_aditim/static/images/blog_post_images/%s.jpg' % file_name)
            # k = int(f.width / 350)
            # height = int(f.height / k)
            # width = int(f.width / k)
            # ff = f.resize((width, height), Image.NEAREST)
            # ext = '.jpg'
            # ff.save(('/home/odoo/website_aditim/static/images/blog_post_images/%s' % file_name) + ext)
            # ff.save(('openerp/addons/website_aditim/static/images/blog_post_images/%s' % file_name) + ext)

            vals['background_image_small'] = file_name + '.jpg'
            vals['background_image'] = 'data:image/jpg;base64,' + vals.get('background_image')

        if vals.get('name_rus'):
            vals['name'] = translit(vals.get('name_rus'), reversed=True)
        month = datetime.date.today().month
        year = datetime.date.today().year
        filter_value = '%s_%s' % (month, year)
        vals['time_filter'] = filter_value
        return super(blog_post, self).write(cr, uid, ids, vals, context=context)

    def create(self, cr, uid, values, context=None):
        month = datetime.date.today().month
        year = datetime.date.today().year
        filter_value = '%s_%s' % (month, year)
        values['time_filter'] = filter_value
        return super(blog_post, self).create(
            cr, uid, values, context=context)


    def _check_for_publication(self, cr, uid, ids, vals, context=None):
        if vals.get('website_published'):
            base_url = self.pool['ir.config_parameter'].get_param(cr, uid, 'web.base.url')
            for post in self.browse(cr, uid, ids, context=context):
                post.blog_id.message_post(
                    body='<p>%(post_publication)s <a href="%(base_url)s/blog/%(blog_slug)s/blog_post/%(post_slug)s">%(post_link)s</a></p>' % {
                        'post_publication': _('A new post %s has been published on the %s blog.') % (
                        post.name, post.blog_id.name),
                        'post_link': _('Click here to access the post.'),
                        'base_url': base_url,
                        'blog_slug': slug(post.blog_id),
                        'post_slug': slug(post),
                    },
                    subtype='website_blog.mt_blog_blog_published',
                    context=context)
            return True
        return False

    def create_mail(self, cr, uid, user_id, partner_id, post_id, template_name, context={}):
        email_template_pool = self.pool.get('email.template')
        template_id = email_template_pool.search(cr, SUPERUSER_ID, [('name', '=', template_name)], context=context)
        mail_id = email_template_pool.send_mail(cr, SUPERUSER_ID, template_id[0], partner_id, force_send=False, raise_exception=True, context=context)
        return mail_id


blog_post()

class time_filter(osv.osv):
    _name = 'blog.time.filter'
    _order = 'month'
    _columns = {
        'value': fields.char('Value'),
        'month': fields.char('Month'),
        'year': fields.char('Year'),
    }


class blog_post_like(osv.osv):
    _name = "blog.like"
    _columns = {
        'user_id': fields.many2one('res.partner', 'User'),
        'blog_id': fields.integer('Blog ID'),
        'like': fields.boolean('Like'),
        'dislike': fields.boolean('Dislike')
    }


blog_post_like()


class res_partner(osv.osv):
    _inherit = ['res.partner']
    _columns = {
        'blog_ids': fields.one2many('blog.like', 'user_id', 'Blog ID')
    }


res_partner()
