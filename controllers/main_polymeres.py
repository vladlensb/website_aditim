# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from __future__ import unicode_literals
import werkzeug
from openerp.http import route
from PIL import Image
import base64
from openerp.addons.website.models.website import slug
from openerp.addons.website_blog.controllers.main import QueryURL
from openerp.addons.auth_signup.res_users import SignupError
from openerp import tools
import logging
import werkzeug.utils
import openerp
from werkzeug.utils import redirect
from openerp import SUPERUSER_ID
from openerp import http
from openerp.http import request
from openerp.addons.web.controllers.main import ensure_db, login_and_redirect
from openerp.tools.translate import _
import datetime
import datetime as dt

_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class PolymeresControllers(http.Controller):
    _post_per_page = 9
    _post_per_page_blog = 9
    _post_comment_per_page = 10
    _case_post_per_page = 5
    _super_user = 1

    def nav_list(self):
        blog_post_obj = request.registry['blog.post']
        groups = blog_post_obj.read_group(
            request.cr, request.uid, [], ['name', 'create_date'],
            groupby="create_date", orderby="create_date desc", context=request.context)
        for group in groups:
            begin_date = datetime.datetime.strptime(group['__domain'][0][2],
                                                    tools.DEFAULT_SERVER_DATETIME_FORMAT).date()
            end_date = datetime.datetime.strptime(group['__domain'][1][2], tools.DEFAULT_SERVER_DATETIME_FORMAT).date()
            group['date_begin'] = '%s' % datetime.date.strftime(begin_date, tools.DEFAULT_SERVER_DATE_FORMAT)
            group['date_end'] = '%s' % datetime.date.strftime(end_date, tools.DEFAULT_SERVER_DATE_FORMAT)
        return groups

    @route(['/polymeres', '/polymeres/blog', '/polymeres/blog/filter/<string:filter>',
            '/polymeres/blog/time_filter/<string:time_filter>', ], type='http',
           auth="public", website=True)
    def polymeres_main_page(self, page=1, **post):

        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_post_pool = pool.get('blog.post')
        blog_tags = request.registry('blog.tag')
        time_filter_name = ''
        filter_name = ''
        blog_tag_ids = blog_tags.search(cr, SUPERUSER_ID, [], context=context)
        blog_tags = blog_tags.browse(cr, SUPERUSER_ID, blog_tag_ids, context=context)
        blog_blog_pool = pool.get('blog.blog')
        blog_blog_ids = blog_blog_pool.search(cr, SUPERUSER_ID, [('website', '=', 'polymeres')], order='name',
                                              context=context)
        blog_blog_objects = blog_blog_pool.browse(cr, SUPERUSER_ID, blog_blog_ids, context=context)
        blog_slider_pool = pool.get('blog.slider')
        blog_slider_ids = blog_slider_pool.search(cr, SUPERUSER_ID,
                                                  [('show', '=', True), ('website', '=', 'polymeres')], context=context)
        blog_slider_objects = blog_slider_pool.browse(cr, uid, blog_slider_ids, context=context)

        if post.get('filter'):
            blog_blog_id = blog_blog_pool.search(cr, uid, [('filter_link', '=', post.get('filter'))], context=context)
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('blog_id', 'in', blog_blog_id)],
                                                  offset=(page - 1) * self._post_per_page, limit=self._post_per_page,
                                                  context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
            blog_post_all = len(blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                         ('blog_id', 'in', blog_blog_id),
                                                                         ('website', '=', 'polymeres')],
                                                      context=context))
            filter_name = str(post.get('filter'))
            pager = request.website.pager(
                url="/polymeres/blog/time_filter/%s" % post.get('filter'),
                total=blog_post_all,
                page=page,
                step=self._post_per_page,
            )
        elif post.get('time_filter'):
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True),
                                                   ('time_filter', '=', post.get('time_filter')),
                                                   ('website', '=', 'polymeres')],
                                                  offset=(page - 1) * self._post_per_page, limit=self._post_per_page,
                                                  context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
            blog_post_all = len(blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                         ('time_filter', '=', post.get('time_filter')),
                                                                         ('website', '=', 'polymeres')],
                                                      context=context))
            time_filter_name = str(post.get('time_filter'))
            pager = request.website.pager(
                url="/polymeres/blog/time_filter/%s" % post.get('time_filter'),
                total=blog_post_all,
                page=page,
                step=self._post_per_page,
            )
        else:
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('website', '=', 'polymeres')],
                                                  offset=(page - 1) * self._post_per_page, limit=self._post_per_page,
                                                  context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
            blog_post_all = len(
                blog_post_pool.search(cr, SUPERUSER_ID,
                                      [('website_published', '=', True), ('website', '=', 'polymeres')],
                                      context=context))

            pager = request.website.pager(
                url="/polymeres/blog",
                total=blog_post_all,
                page=page,
                step=self._post_per_page,
            )

        time_filter_pool = pool.get('blog.time.filter')
        time_filter_ids = time_filter_pool.search(cr, uid, [], context=context)
        time_filters = time_filter_pool.browse(cr, uid, time_filter_ids, context=context)
        try:
            num_slider = len(blog_slider_objects)
        except:
            num_slider = 0

        all_blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('website', '=', 'polymeres')],
                                                  context=context)
        have_more = 0
        if len(all_blog_post_ids) > self._post_per_page:
            have_more = 1

        values = {
            'blog_data': blog_post_objects,
            'blog_tags': blog_tags,
            'pager': pager,
            'filter': post.get('filter'),
            'filter_name': filter_name,
            'blog_blog_objects': blog_blog_objects,
            'slider': blog_slider_objects,
            'num_slider': num_slider,
            'time_filters': time_filters,
            'time_filter_name': time_filter_name,
            'have_more': have_more,
            'polymeres': '1',
        }
        return request.website.render('website.aditim_main', values)

    @http.route(["/polymeres/blog/<model('blog.blog'):blog>/blog_post/<model('blog.post'):blog_post>"], type='http',
                auth="public", website=True)
    def blog_post(self, blog, blog_post, tag_id=None, page=1, enable_editor=None, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        tag_obj = request.registry['blog.tag']
        blog_post_obj = request.registry['blog.post']
        date_begin, date_end = post.get('date_begin'), post.get('date_end')

        pager_url = "/blog/%s" % blog_post.id

        site_url = request.httprequest.url_root
        pager = request.website.pager(
            url=pager_url,
            total=len(blog_post.website_message_ids),
            page=page,
            step=self._post_comment_per_page,
            scope=7
        )

        tag = None
        if tag_id:
            tag = request.registry['blog.tag'].browse(request.cr, request.uid, int(tag_id), context=request.context)
        post_url = QueryURL('', ['blogpost'], blogpost=blog_post, tag_id=tag_id, date_begin=date_begin,
                            date_end=date_end)
        blog_url = QueryURL('', ['blog', 'tag'], blog=blog_post.blog_id, tag=tag, date_begin=date_begin,
                            date_end=date_end)

        if not blog_post.blog_id.id == blog.id:
            return request.redirect("/polymeres/blog/%s/blog_post/%s" % (slug(blog_post.blog_id), slug(blog_post)))

        tags = tag_obj.browse(cr, uid, tag_obj.search(cr, uid, [], context=context), context=context)
        all_post_ids = blog_post_obj.search(cr, uid, [('blog_id', '=', blog.id), ('website_published', '=', True),
                                                      ('website', '=', 'polymeres')], order="id", context=context)
        len_all_post_ids = len(all_post_ids)
        if len_all_post_ids > 1:
            k = i = 0
            for post_id in all_post_ids:
                if post_id == blog_post.id:
                    k = i
                i += 1
            try:
                next_post_id = all_post_ids[k + 1]
            except:
                next_post_id = all_post_ids[0]
        else:
            next_post_id = []
        next_post = blog_post_obj.browse(cr, uid, next_post_id, context=context)

        blog_blog_pool = pool.get('blog.blog')
        blog_blog_ids = blog_blog_pool.search(cr, SUPERUSER_ID, [('website', '=', 'polymeres')], order='name',
                                              context=context)
        blog_blog_objects = blog_blog_pool.browse(cr, SUPERUSER_ID, blog_blog_ids, context=context)

        blog_comments_pool = pool.get('blog.comment')
        blog_comments_ids = blog_comments_pool.search(cr, uid, [('blog_post_id', '=', int(blog_post.id))],
                                                      context=context)
        blog_comments = blog_comments_pool.browse(cr, uid, blog_comments_ids, context=context)
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)

        values = {
            'tags': tags,
            'tag': tag,
            'blog': blog,
            'blog_post': blog_post,
            'main_object': blog_post,
            'nav_list': self.nav_list(),
            'enable_editor': enable_editor,
            'next_post': next_post,
            'date': date_begin,
            'post_url': post_url,
            'blog_url': blog_url,
            'pager': pager,
            'comments': blog_comments,
            'site_url': site_url,
            'blog_blog_objects': blog_blog_objects,
            'res_users_object': res_users_object,
            'polymeres': '1'
        }

        blog_post_pool = pool.get('blog.post')
        blog_post_ids = blog_post_pool.search(cr, uid,
                                              [('website_published', '=', True), ('website', '=', 'polymeres')],
                                              context=context)
        blog_post_objects = blog_post_pool.browse(cr, uid, blog_post_ids, context=context)
        blog_posts = blog_post_objects[:3]
        values['blog_posts'] = blog_posts

        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        partner_id = res_users_object.partner_id.id
        values['partner_id'] = partner_id

        response = request.website.render("website.aditim_blog_post", values)

        request.session[request.session_id] = request.session.get(request.session_id, [])
        if not (blog_post.id in request.session[request.session_id]):
            request.session[request.session_id].append(blog_post.id)
            # Increase counter
            blog_post_obj.write(cr, SUPERUSER_ID, [blog_post.id], {
                'visits': blog_post.visits + 1,
            }, context=context)
        return response

    @http.route(["/polymeres/blog/upload_more"], type='json', auth="public", website=True)
    def polymeres_polymeres_upload_more(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        data = ''
        blog_post_pool = pool.get('blog.post')
        blog_blog_pool = pool.get('blog.blog')
        count = post.get('count')
        if post.get('filter_name'):
            blog_blog_id = blog_blog_pool.search(cr, uid, [('filter_link', '=', post.get('filter_name')),
                                                           ('website', '=', 'polymeres')],
                                                 context=context)
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('blog_id', 'in', blog_blog_id),
                                                   ('website', '=', 'polymeres')],
                                                  offset=count, limit=self._post_per_page, context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
        elif post.get('time_filter_name'):
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                     ('time_filter', '=', post.get('time_filter')),
                                                                     ('website', '=', 'polymeres')],
                                                  offset=count, limit=self._post_per_page, context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
        else:

            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('website', '=', 'polymeres')],
                                                  offset=count, limit=self._post_per_page, context=context)

            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)

        all_blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('website', '=', 'polymeres')],
                                                  context=context)
        have_more = 0
        if len(all_blog_post_ids) > (len(blog_post_ids) + count):
            have_more = 1

        for blog_post in blog_post_objects:

            if not blog_post.blog_id.other_prev:
                r = '<div class="item">' \
                    '<a href="/polymeres/blog/%s/blog_post/%s/#blog_content">' \
                    '<span class="img_wrap">' \
                    '<img alt="" src="/website_aditim/static/images/blog_post_images/%s"/>' \
                    '</span>' \
                    '<span class="text_wrap">' \
                    '<span class="category">%s</span>' \
                    '</span>' \
                    '<span class="title">%s</span>' \
                    '<span class="text">%s</span>' \
                    '<span class="clear"/>' \
                    '</a>' \
                    '</div>' % (
                        slug(blog_post.blog_id), slug(blog_post), blog_post.background_image_small,
                        blog_post.blog_id.name,
                        blog_post.name, blog_post.subtitle)
            else:
                r = '<div class="item">' \
                    '<a href="/polymeres/blog/%s/blog_post/%s/#blog_content" class="right_blog">' \
                    '<span class="img_wrap">' \
                    '<img alt="" src="/website_aditim/static/images/blog_post_images/%s"/>' \
                    '</span>' \
                    '<span class="text_wrap">' \
                    '<span class="category">%s</span>' \
                    '</span>' \
                    '<span class="title">%s</span>' \
                    '<span class="text">%s</span>' \
                    '<span class="clear"/>' \
                    '</a>' \
                    '</div>' % (
                        slug(blog_post.blog_id), slug(blog_post), blog_post.background_image_small,
                        blog_post.blog_id.name,
                        blog_post.name, blog_post.subtitle)
            data += r
        return {'data': data, 'have_more': have_more}

    @http.route('/polymeres/lk', type='http', auth="user", website=True)
    def polymeres_profile(self):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        return request.website.render("website.aditim_profile", {'polymeres': '1'})

    @http.route('/polymeres/update_profile', type='http', auth="user", website=True)
    def polymeres_update_profile(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        if res_users_object:
            partner_id = res_users_object.partner_id.id
            values = {'first_name': post.get('name'),
                           'last_name': post.get('second_name'),
                           'company_name': post.get('company'),
                           'email': post.get('mail')}
            res_partner_pool = pool.get('res.partner')
            res_partner_pool.write(cr, SUPERUSER_ID, partner_id, values, context=context)
        return request.redirect('/polymeres/lk')

    @http.route('/polymeres/blog/create_new_post_title', type='http', auth="user", website=True)
    def polymeres_blog_post_create_title(self):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_blog_pool = pool.get('blog.blog')
        blog_blog_ids = blog_blog_pool.search(cr, uid, [('website', '=', 'polymeres')], order='name', context=context)
        blog_blog_objects = blog_blog_pool.browse(cr, uid, blog_blog_ids, context=context)
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        author = res_users_object.partner_id
        blog_post_pool = pool.get('blog.post')
        blog_post_ids = blog_post_pool.search(cr, uid, [('author_id', '=', author.id), ('website', '=', 'polymeres')],
                                              context=context)
        try:
            description = blog_post_pool.browse(cr, uid, blog_post_ids[-1], context=context).description
        except:
            description = ''
        values = {'blog_blog_objects': blog_blog_objects, 'author': author, 'description': description,
                  'polymeres': '1'}
        return request.website.render("website.aditim_blog_post_create", values)

    @http.route('/polymeres/blog/create_new_post_content', type='http', auth="user", website=True)
    def polymeres_blog_post_create_content(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_post_pool = pool.get('blog.post')
        try:
            file_name = post.get('image').filename
            if file_name:
                background_image_small = background_image = post.get('image')
                file_name_dict = file_name.split('.')
                ext = '.' + file_name_dict[-1]
                file_name = '%s_%s' % (file_name_dict[0], '_temp')
                # background_image.save(('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/website_aditim/static/polymeres/images/blog_post_images/%s' % file_name) + ext)
                background_image.save(('/home/odoo/website_aditim/static/images/blog_post_images/%s' % file_name) + ext)

                # f = Image.open('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/website_aditim/static/polymeres/images/blog_post_images/%s%s' % (file_name, ext))
                f = Image.open('/home/odoo/website_aditim/static/images/blog_post_images/%s%s' % (file_name, ext))

                # with open('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/website_aditim/static/polymeres/images/blog_post_images/%s%s' % (file_name, ext),
                #           "rb") as imageFile:
                with open('/home/odoo/website_aditim/static/images/blog_post_images/%s%s' % (file_name, ext),
                          "rb") as imageFile:
                    background_image = 'data:image/jpg;base64,' + base64.b64encode(imageFile.read())

                if not background_image:
                    blog_blog_pool = pool.get('blog.blog')
                    blog_blog_object = blog_blog_pool.browse(cr, SUPERUSER_ID, int(post.get('blog_blog')),
                                                             context=context)
                    try:
                        background_image = blog_blog_object.background_image
                    except:
                        background_image = ''
        except:
            return request.redirect("/polymeres/blog/create_new_post_title")

        month = datetime.date.today().month
        year = datetime.date.today().year
        filter_value = '%s_%s' % (month, year)

        time_filter_pool = pool.get('blog.time.filter')
        time_filter_ids = time_filter_pool.search(cr, uid, [('value', '=', filter_value)], context=context)
        if not time_filter_ids:
            values = {
                'value': str(filter_value),
                'month': str(month),
                'year': str(year),
            }
            time_filter_pool.create(cr, uid, values, context=context)
        value = {
            'blog_id': post.get('blog_blog'),
            'name_rus': post.get('post_title'),
            'subtitle': post.get('post_subtitle'),
            'content': '',
            'website_published': False,
            'time_filter': filter_value,
            'website': 'polymeres'

        }
        try:
            value['background_image'] = background_image
            value['background_image_small'] = background_image_small
        except:
            pass
        new_blog_post_id = blog_post_pool.create(cr, uid, value, context=context)
        new_blog_post = request.registry['blog.post'].browse(cr, uid, new_blog_post_id, context=context)
        return werkzeug.utils.redirect(
            "/polymeres/blog/%s/blog_post/%s?enable_editor=1" % (slug(new_blog_post.blog_id), slug(new_blog_post)))

    @http.route(['/polymeres/blog/post/comment/<model("blog.post"):blog_post_object>'], type='http', auth="public",
                website=True)
    def polymeres_save_blog_comments(self, blog_post_object=None, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        if not form_data or not form_data.get('g-recaptcha-response'):
            return redirect(
                "/polymeres/blog/%s/blog_post/%s" % (slug(blog_post_object.blog_id), slug(blog_post_object)))
        email = form_data.get('mail')
        values = {
            'user_id': uid,
            'blog_post_id': blog_post_object.id,
            'name': form_data.get('name'),
            'email': email,
            'text_comment': form_data.get('blog_comment'),
        }

        blog_comments_pool = pool.get('blog.comment')
        blog_comments_pool.create(cr, uid, values, context=context)

        pool.get('blog.post').write(cr, 1, blog_post_object.id, {
            'comment': blog_post_object.comment + 1,
            'last_time_comment': datetime.datetime.today()
        }, context=context)

        crm_lead_pool = pool.get('crm.lead')
        crm_lead_ids = crm_lead_pool.search(cr, 1, [('email_from', '=', email)], context=context)
        if not crm_lead_ids:
            crm_lead_pool.create(cr, 1, {
                'name': form_data.get('name'),
                'partner_name': form_data.get('name'),
                'email_from': email,
            }, context=context)
        return redirect("/polymeres/blog/%s/blog_post/%s" % (slug(blog_post_object.blog_id), slug(blog_post_object)))

    @http.route('/polymeres/ask_us', type='http', auth="public", website=True)
    def polymeres_ask_us(self, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        blog_post_pool = pool.get('blog.post')
        question_ids = blog_post_pool.search(cr, uid, [('ask', '=', True), ('website_published', '=', True)],
                                             context=context)
        questions = blog_post_pool.browse(cr, uid, question_ids, context=context)
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)

        values = {
            'res_users_object': res_users_object,
            'questions': questions,
            'polymeres': '1'
        }
        return request.website.render("website.aditim_ask_us", values)

    @http.route('/polymeres/create_ask', type='http', auth="user", website=True)
    def polymeres_create_ask(self, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_post_pool = pool.get('blog.post')
        question_ids = blog_post_pool.search(cr, uid, [('ask', '=', True), ('website_published', '=', True)],
                                             context=context)
        questions = blog_post_pool.browse(cr, uid, question_ids, context=context)
        values = {
            'questions': questions,
            'polymeres': '1'
        }

        if not form_data or not form_data.get('g-recaptcha-response'):
            return request.website.render("website.aditim_ask_us", values)

        values = {
            'user_id': uid,
            'title': form_data.get('title'),
            'question': form_data.get('question'),
            'name': form_data.get('name'),
            'email': form_data.get('email'),
            'company': form_data.get('company'),
            'questions': questions,
            'website': 'polymeres',
        }
        question_pool = pool.get('question')
        question_id = question_pool.create(cr, uid, values, context=context)

        blog_post_pool = pool.get('blog.post')
        month = dt.date.today().month
        year = dt.date.today().year
        filter_value = '%s_%s' % (month, year)

        time_filter_pool = pool.get('blog.time.filter')
        time_filter_ids = time_filter_pool.search(cr, uid, [('value', '=', filter_value)], context=context)
        if not time_filter_ids:
            values = {
                'value': str(filter_value),
                'month': str(month),
                'year': str(year),
            }
            time_filter_pool.create(cr, uid, values, context=context)
        blog_blog_pool = pool.get('blog.blog')
        blog_blog_id = blog_blog_pool.search(cr, uid, [('ask', '=', True), ('website', '=', 'polymeres')],
                                             context=context)
        if not blog_blog_id:
            blog_blog_id = blog_blog_pool.create(cr, 1, {
                'name': 'Вы спрашивали',
                'filter_link': 'You_ask',
                'ask': True,
                'website': 'polymeres'
            })
        else:
            blog_blog_id = blog_blog_id[0]

        value = {
            'blog_id': blog_blog_id,
            'name_rus': form_data.get('title'),
            'subtitle': form_data.get('question'),
            'content': '',
            'website_published': False,
            'time_filter': filter_value,
            'question_id': question_id,
            'ask': True,
            'website': 'polymeres',
        }
        blog_post_pool.create(cr, uid, value, context=context)

        return redirect("/polymeres/ask/%s" % question_id)

    @http.route(["/polymeres/ask/<model('question'):question>"], type='http', auth="public", website=True)
    def polymeres_ask(self, question, enable_editor=None, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        blog_post_pool = pool.get('blog.post')
        question_ids = blog_post_pool.search(cr, uid, [('ask', '=', True), ('website_published', '=', True)],
                                             context=context)
        questions = blog_post_pool.browse(cr, uid, question_ids, context=context)
        values = {
            'question': question,
            'questions': questions,
            'polymeres': '1'
        }
        return request.website.render("website.aditim_ask_page", values)

    @http.route('/polymeres/logout', type='http', auth="none")
    def polymeres_logout(self):
        request.session.logout(keep_db=True)
        return redirect("/polymeres/blog")

    @http.route('/polymeres/about_us', type='http', auth="public", website=True)
    def polymeres_about_us(self):
        return request.website.render("website.aditim_about_us", {'polymeres': '1'})

    @http.route('/polymeres/policy', type='http', auth="public", website=True)
    def polymeres_policy(self):
        return request.website.render("website.aditim_policy", {'polymeres': '1'})

    @http.route('/polymeres/rules', type='http', auth="public", website=True)
    def polymeres_rules(self):
        return request.website.render("website.aditim_rule", {'polymeres': '1'})

    @http.route('/polymeres/chg_pass', type='http', auth="user", website=True)
    def polymeres_change_password(self, **post):
        old_password = post.get('old_password')
        new_password = post.get('new_password')
        confirm_password = post.get('confirm_password')

        if not (old_password.strip() and new_password.strip() and confirm_password.strip()):
            values = {
                'error': 'Не правильно заполненая форма',
                'polymeres': '1',
            }
            return request.website.render("website.aditim_profile", values)
        if new_password != confirm_password:
            values = {
                'error': 'Новый пароль и пароль подтверждения разные',
                'polymeres': '1',
            }
            return request.website.render("website.aditim_profile", values)
        try:
            if request.session.model('res.users').change_password(old_password, new_password):
                values = {
                    'message': 'Пароль изменено',
                    'polymeres': '1',
                }
                return request.website.render("website.aditim_profile", values)
        except Exception:
            values = {
                'error': 'Неправильный старый пароль',
                'polymeres': '1',
            }
            return request.website.render("website.aditim_profile", values)

    @http.route('/polymeres/search', type='http', auth="public", website=True, method='get')
    def site_search(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        blog_post_pool = pool.get('blog.post')
        search_value = post.get('search')
        if not search_value:
            pass
        else:
            search_value = search_value.lower()
            blog_post_ids = blog_post_pool.search(cr, uid, [('website_published', '=', True),('website', '=', 'polymeres')], context=context)
            blog_post_objects = blog_post_pool.browse(cr, uid, blog_post_ids, context=context)
            ids = []
            for blog_post in blog_post_objects:
                if search_value in blog_post.name.lower() and blog_post.id not in ids:
                    ids.append(blog_post.id)
                if search_value in blog_post.subtitle.lower() and blog_post.id not in ids:
                    ids.append(blog_post.id)

            search_value_list = search_value.split(' ')

            for blog_post in blog_post_objects:
                for search_value_l in search_value_list:
                    if search_value_l in blog_post.name.lower() and blog_post.id not in ids:
                        ids.append(blog_post.id)
                    if search_value_l in blog_post.subtitle.lower() and blog_post.id not in ids:
                        ids.append(blog_post.id)
            blog_post_objects = blog_post_pool.browse(cr, uid, ids,
                                                      context=context)
            values = {
                'blog_data': blog_post_objects,
                'polymeres': '1',
            }
            return request.website.render('website.aditim_search', values)