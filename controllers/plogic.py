# -*- coding: UTF-8 -*-


import json
import requests
import openerp.http
import openerp


# Получает ответ от requests и проверяет если код не 200, то кидает исключение.
def raiseIfRequestsStatusCodeIsNot200(requests_response):
    if requests_response.status_code != 200:
        raise RuntimeError(
            u'Непредвиденный ответ от сайта на который посылается запрос. '
            u'Полученный HTTP код: {0}'.format(requests_response.status_code))


# Получая на вход временный код, делает запрос в вконтакте и получает email и access_token
# Реализует вот этот запрос из доки: http://pvoytko.ru/jx/u3o7KLOEHf
# Используется при авторизации-регистрции с помощью вконтакте на странице входа.
def getVkAccessTokenByCode(temp_code):
    print 'temp_code  >>>>>>>>>>>>>>>>>>>>', temp_code
    url = "https://oauth.vk.com/access_token?client_id={cid}&client_secret={cs}&redirect_uri={ru}&code={cd}&scope=email".format(
        cid="5609128",
        cs="UgnJtUkTMsHyfROEcY86",
        ru=getVkOAuthRedirectUrl(),
        cd=temp_code,
    )

    # cid = "5758515",
    # cs = "lK7pPkx777K1fLPp5NYI",
    resp = requests.get(url)
    raiseIfRequestsStatusCodeIsNot200(resp)
    resp_json = json.loads(resp.content)
    return resp_json['access_token'], resp_json['user_id'], resp_json['email']


def getVkOAuthRedirectUrl():
    return openerp.http.request.httprequest.url_root + 'auth_oauth/signin?AD_AUTH2_VK'


# По access_token получить ФИО для вкнтакте
def getVkFioByAcessToken(access_token):
    url = "https://api.vk.com/method/users.get?access_token={at}&v=5.60".format(
        at=access_token,
    )
    resp = requests.get(url)
    raiseIfRequestsStatusCodeIsNot200(resp)
    resp_dict = json.loads(resp.content)
    return resp_dict['response'][0]['last_name'] + u' ' + \
           resp_dict['response'][0]['first_name']


# возвращает модель юзера из БЩ по емейл, если он есть, или None, если его нет.
def getUserFromDbByEmail(request, user_email):
    res_users_registry = request.registry('res.users')
    print 1234567890
    res_users = request.registry.get('res.users')
    user_ids = res_users.pool.get('res.users').search(
        request.cr, openerp.SUPERUSER_ID, [('login', '=', user_email)],
        context=request.context
    )
    acc_existing = None
    if user_ids:
        acc_existing = res_users.pool.get('res.users').browse(request.cr,
                                                              openerp.SUPERUSER_ID,
                                                              user_ids[0],
                                                              context=request.context)
    return acc_existing
