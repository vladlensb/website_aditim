# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from __future__ import unicode_literals
import werkzeug
import json
from openerp.http import route
from PIL import Image
import base64
from openerp.addons.website.models.website import slug
from openerp.addons.website_blog.controllers.main import QueryURL
from openerp.addons.auth_signup.res_users import SignupError
from openerp import tools
import logging
import werkzeug.utils
import openerp
from werkzeug.utils import redirect
from openerp import SUPERUSER_ID
from openerp import http
from openerp.http import request
from openerp.addons.web.controllers.main import ensure_db, login_and_redirect, set_cookie_and_redirect
from openerp.tools.translate import _
import datetime
import datetime as dt
import urllib
import plogic
import functools
from openerp.addons.auth_oauth.controllers.main import OAuthController as OAController
from linkedin import linkedin
from openerp.modules.registry import RegistryManager



_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class Website(openerp.addons.web.controllers.main.Home):
    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        return request.redirect('/aditim')


class AditimControllers(http.Controller):
    _post_per_page = 9
    _post_per_page_blog = 9
    _post_comment_per_page = 10
    _case_post_per_page = 5
    _super_user = 1

    def nav_list(self):
        blog_post_obj = request.registry['blog.post']
        groups = blog_post_obj.read_group(
            request.cr, request.uid, [], ['name', 'create_date'],
            groupby="create_date", orderby="create_date desc", context=request.context)
        for group in groups:
            begin_date = datetime.datetime.strptime(group['__domain'][0][2],
                                                    tools.DEFAULT_SERVER_DATETIME_FORMAT).date()
            end_date = datetime.datetime.strptime(group['__domain'][1][2], tools.DEFAULT_SERVER_DATETIME_FORMAT).date()
            group['date_begin'] = '%s' % datetime.date.strftime(begin_date, tools.DEFAULT_SERVER_DATE_FORMAT)
            group['date_end'] = '%s' % datetime.date.strftime(end_date, tools.DEFAULT_SERVER_DATE_FORMAT)
        return groups

    @route(['/aditim', '/aditim/blog', '/aditim/blog/filter/<string:filter>',
            '/aditim/blog/time_filter/<string:time_filter>', ], type='http',
           auth="public", website=True)
    def main_page(self, page=1, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_post_pool = pool.get('blog.post')
        blog_tags = request.registry('blog.tag')
        time_filter_name = ''
        filter_name = ''
        blog_tag_ids = blog_tags.search(cr, SUPERUSER_ID, [], context=context)
        blog_tags = blog_tags.browse(cr, SUPERUSER_ID, blog_tag_ids, context=context)
        blog_blog_pool = pool.get('blog.blog')
        blog_blog_ids = blog_blog_pool.search(cr, SUPERUSER_ID, [('website', '=', 'aditim')], order='name',
                                              context=context)
        blog_blog_objects = blog_blog_pool.browse(cr, SUPERUSER_ID, blog_blog_ids, context=context)

        blog_slider_pool = pool.get('blog.slider')
        blog_slider_ids = blog_slider_pool.search(cr, SUPERUSER_ID, [('show', '=', True), ('website', '=', 'aditim')],
                                                  context=context)
        blog_slider_objects = blog_slider_pool.browse(cr, uid, blog_slider_ids, context=context)

        if post.get('filter'):
            blog_blog_id = blog_blog_pool.search(cr, uid, [('filter_link', '=', post.get('filter'))], context=context)
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('blog_id', 'in', blog_blog_id)],
                                                  offset=(page - 1) * self._post_per_page, limit=self._post_per_page,
                                                  context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
            blog_post_all = len(blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                         ('blog_id', 'in', blog_blog_id),
                                                                         ('website', '=', 'aditim')],
                                                      context=context))
            filter_name = str(post.get('filter'))
            pager = request.website.pager(
                url="/aditim/blog/time_filter/%s" % post.get('filter'),
                total=blog_post_all,
                page=page,
                step=self._post_per_page,
            )
        elif post.get('time_filter'):
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True),
                                                   ('time_filter', '=', post.get('time_filter')),
                                                   ('website', '=', 'aditim')],
                                                  offset=(page - 1) * self._post_per_page, limit=self._post_per_page,
                                                  context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
            blog_post_all = len(blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                         ('time_filter', '=', post.get('time_filter')),
                                                                         ('website', '=', 'aditim')], context=context))
            time_filter_name = str(post.get('time_filter'))
            pager = request.website.pager(
                url="/aditim/blog/time_filter/%s" % post.get('time_filter'),
                total=blog_post_all,
                page=page,
                step=self._post_per_page,
            )
        else:
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                     ('website', '=', 'aditim')],
                                                  offset=(page - 1) * self._post_per_page, limit=self._post_per_page,
                                                  context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
            blog_post_all = len(
                blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                         ('website', '=', 'aditim')], context=context))

            pager = request.website.pager(
                url="/aditim/blog",
                total=blog_post_all,
                page=page,
                step=self._post_per_page,
            )

        time_filter_pool = pool.get('blog.time.filter')
        time_filter_ids = time_filter_pool.search(cr, uid, [], context=context)
        time_filters = time_filter_pool.browse(cr, uid, time_filter_ids, context=context)
        try:
            num_slider = len(blog_slider_objects)
        except:
            num_slider = 0

        all_blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                     ('website', '=', 'aditim')], context=context)
        have_more = 0
        if len(all_blog_post_ids) > self._post_per_page:
            have_more = 1

        values = {
            'blog_data': blog_post_objects,
            'blog_tags': blog_tags,
            'pager': pager,
            'filter': post.get('filter'),
            'filter_name': filter_name,
            'blog_blog_objects': blog_blog_objects,
            'slider': blog_slider_objects,
            'num_slider': num_slider,
            'time_filters': time_filters,
            'time_filter_name': time_filter_name,
            'have_more': have_more,
            'aditim': '1',
        }
        return request.website.render('website.aditim_main', values)

    @http.route(["/aditim/blog/<model('blog.blog'):blog>/blog_post/<model('blog.post'):blog_post>"],
                type='http',                auth="public", website=True)
    def blog_post(self, blog, blog_post, tag_id=None,
                  page=1, enable_editor=None, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        tag_obj = request.registry['blog.tag']
        blog_post_obj = request.registry['blog.post']
        date_begin, date_end = post.get('date_begin'), post.get('date_end')

        pager_url = "/blog/%s" % blog_post.id

        site_url = request.httprequest.url_root
        pager = request.website.pager(
            url=pager_url,
            total=len(blog_post.website_message_ids),
            page=page,
            step=self._post_comment_per_page,
            scope=7
        )

        tag = None
        if tag_id:
            tag = request.registry['blog.tag'].browse(request.cr, request.uid, int(tag_id), context=request.context)
        post_url = QueryURL('', ['blogpost'], blogpost=blog_post, tag_id=tag_id, date_begin=date_begin,
                            date_end=date_end)
        blog_url = QueryURL('', ['blog', 'tag'], blog=blog_post.blog_id, tag=tag, date_begin=date_begin,
                            date_end=date_end)

        if not blog_post.blog_id.id == blog.id:
            return request.redirect("/aditim/blog/%s/blog_post/%s" % (slug(blog_post.blog_id), slug(blog_post)))

        tags = tag_obj.browse(cr, uid, tag_obj.search(cr, uid, [], context=context), context=context)
        all_post_ids = blog_post_obj.search(cr, uid, [('blog_id', '=', blog.id), ('website_published', '=', True),
                                                      ('website', '=', 'aditim')], order="id", context=context)
        len_all_post_ids = len(all_post_ids)
        if len_all_post_ids > 1:
            k = i = 0
            for post_id in all_post_ids:
                if post_id == blog_post.id:
                    k = i
                i += 1
            try:
                next_post_id = all_post_ids[k + 1]
            except:
                next_post_id = all_post_ids[0]
        else:
            next_post_id = []
        next_post = blog_post_obj.browse(cr, uid, next_post_id, context=context)

        blog_blog_pool = pool.get('blog.blog')
        blog_blog_ids = blog_blog_pool.search(cr, SUPERUSER_ID, [('website', '=', 'aditim')], order='name',
                                              context=context)
        blog_blog_objects = blog_blog_pool.browse(cr, SUPERUSER_ID, blog_blog_ids, context=context)

        blog_comments_pool = pool.get('blog.comment')
        blog_comments_ids = blog_comments_pool.search(cr, uid, [('blog_post_id', '=', int(blog_post.id))],
                                                      context=context)
        blog_comments = blog_comments_pool.browse(cr, uid, blog_comments_ids, context=context)
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)

        values = {
            'tags': tags,
            'tag': tag,
            'blog': blog,
            'blog_post': blog_post,
            'main_object': blog_post,
            'nav_list': self.nav_list(),
            'enable_editor': enable_editor,
            'next_post': next_post,
            'date': date_begin,
            'post_url': post_url,
            'blog_url': blog_url,
            'pager': pager,
            'comments': blog_comments,
            'site_url': site_url,
            'blog_blog_objects': blog_blog_objects,
            'res_users_object': res_users_object,
            'aditim': '1'
        }

        blog_post_pool = pool.get('blog.post')
        blog_post_ids = blog_post_pool.search(cr, uid, [('website_published', '=', True),
                                                        ('website', '=', 'aditim')], context=context)
        blog_post_objects = blog_post_pool.browse(cr, uid, blog_post_ids, context=context)
        blog_posts = blog_post_objects[:3]
        values['blog_posts'] = blog_posts

        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        partner_id = res_users_object.partner_id.id
        values['partner_id'] = partner_id

        response = request.website.render("website.aditim_blog_post", values)

        request.session[request.session_id] = request.session.get(request.session_id, [])
        if not (blog_post.id in request.session[request.session_id]):
            request.session[request.session_id].append(blog_post.id)
            # Increase counter
            blog_post_obj.write(cr, SUPERUSER_ID, [blog_post.id], {
                'visits': blog_post.visits + 1,
            }, context=context)
        return response

    @http.route(["/aditim/blog/upload_more"], type='json', auth="public", website=True)
    def upload_more(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        data = ''
        blog_post_pool = pool.get('blog.post')
        blog_blog_pool = pool.get('blog.blog')
        count = post.get('count')
        if post.get('filter_name'):
            blog_blog_id = blog_blog_pool.search(cr, uid, [('filter_link', '=', post.get('filter_name')),
                                                           ('website', '=', 'aditim')],
                                                 context=context)
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID,
                                                  [('website_published', '=', True), ('blog_id', 'in', blog_blog_id),
                                                   ('website', '=', 'aditim')],
                                                  offset=count, limit=self._post_per_page, context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
        elif post.get('time_filter_name'):
            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                     ('time_filter', '=', post.get('time_filter')),
                                                                     ('website', '=', 'aditim')],
                                                  offset=count, limit=self._post_per_page, context=context)
            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)
        else:

            blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                     ('website', '=', 'aditim')], offset=count,
                                                  limit=self._post_per_page, context=context)

            blog_post_objects = blog_post_pool.browse(cr, SUPERUSER_ID, blog_post_ids, context=context)

        all_blog_post_ids = blog_post_pool.search(cr, SUPERUSER_ID, [('website_published', '=', True),
                                                                     ('website', '=', 'aditim')], context=context)
        have_more = 0
        if len(all_blog_post_ids) > (len(blog_post_ids) + count):
            have_more = 1

        for blog_post in blog_post_objects:

            if not blog_post.blog_id.other_prev:
                r = '<div class="item">' \
                    '<a href="/aditim/blog/%s/blog_post/%s/#blog_content">' \
                    '<span class="img_wrap">' \
                    '<img alt="" src="/website_aditim/static/images/blog_post_images/%s"/>' \
                    '</span>' \
                    '<span class="text_wrap">' \
                    '<span class="category">%s</span>' \
                    '</span>' \
                    '<span class="title">%s</span>' \
                    '<span class="text">%s</span>' \
                    '<span class="clear"/>' \
                    '</a>' \
                    '</div>' % (
                        slug(blog_post.blog_id), slug(blog_post), blog_post.background_image_small,
                        blog_post.blog_id.name,
                        blog_post.name, blog_post.subtitle)
            else:
                r = '<div class="item">' \
                    '<a href="/aditim/blog/%s/blog_post/%s/#blog_content" class="right_blog">' \
                    '<span class="img_wrap">' \
                    '<img alt="" src="/website_aditim/static/images/blog_post_images/%s"/>' \
                    '</span>' \
                    '<span class="text_wrap">' \
                    '<span class="category">%s</span>' \
                    '</span>' \
                    '<span class="title">%s</span>' \
                    '<span class="text">%s</span>' \
                    '<span class="clear"/>' \
                    '</a>' \
                    '</div>' % (
                        slug(blog_post.blog_id), slug(blog_post), blog_post.background_image_small,
                        blog_post.blog_id.name,
                        blog_post.name, blog_post.subtitle)
            data += r
        return {'data': data, 'have_more': have_more}

    @http.route('/aditim/lk', type='http', auth="user", website=True)
    def profile(self):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        return request.website.render("website.aditim_profile", {'aditim': '1'})

    @http.route('/aditim/update_profile', type='http', auth="user", website=True)
    def update_profile(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        if res_users_object:
            partner_id = res_users_object.partner_id.id
            values = {'first_name': post.get('name'),
                           'last_name': post.get('second_name'),
                           'company_name': post.get('company'),
                           'email': post.get('mail')}
            res_partner_pool = pool.get('res.partner')
            res_partner_pool.write(cr, SUPERUSER_ID, partner_id, values, context=context)
        return request.redirect('/aditim/lk')

    @http.route('/aditim/blog/create_new_post_title', type='http', auth="user", website=True)
    def blog_post_create_title(self):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_blog_pool = pool.get('blog.blog')
        blog_blog_ids = blog_blog_pool.search(cr, uid, [('website', '=', 'aditim')], order='name', context=context)
        blog_blog_objects = blog_blog_pool.browse(cr, uid, blog_blog_ids, context=context)
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        author = res_users_object.partner_id
        blog_post_pool = pool.get('blog.post')
        blog_post_ids = blog_post_pool.search(cr, uid, [('author_id', '=', author.id), ('website', '=', 'aditim')],
                                              context=context)
        try:
            description = blog_post_pool.browse(cr, uid, blog_post_ids[-1], context=context).description
        except:
            description = ''
        values = {'blog_blog_objects': blog_blog_objects, 'author': author, 'description': description, 'aditim': '1'}
        return request.website.render("website.aditim_blog_post_create", values)

    @http.route('/aditim/blog/create_new_post_content', type='http', auth="user", website=True)
    def blog_post_create_content(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_post_pool = pool.get('blog.post')
        # try:
        file_name = post.get('image').filename
        if file_name:
            background_image_small = background_image = post.get('image')
            file_name_dict = file_name.split('.')
            ext = '.' + file_name_dict[-1]
            file_name = '%s_%s' % (file_name_dict[0], '_temp')

            # background_image.save(('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/website_aditim/static/aditim/images/blog_post_images/%s' % file_name) + ext)
            background_image.save(('/home/odoo/website_aditim/static/images/blog_post_images/%s' % file_name) + ext)

            # f = Image.open('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/website_aditim/static/aditim/images/blog_post_images/%s%s' % (file_name, ext))
            f = Image.open('/home/odoo/website_aditim/static/images/blog_post_images/%s%s' % (file_name, ext))

            # with open('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/website_aditim/static/aditim/images/blog_post_images/%s%s' % (file_name, ext),
            #           "rb") as imageFile:
            with open('/home/odoo/website_aditim/static/images/blog_post_images/%s%s' % (file_name, ext),
                      "rb") as imageFile:
                background_image = 'data:image/jpg;base64,' + base64.b64encode(imageFile.read())

            if not background_image:
                blog_blog_pool = pool.get('blog.blog')
                blog_blog_object = blog_blog_pool.browse(cr, SUPERUSER_ID, int(post.get('blog_blog')),
                                                         context=context)
                try:
                    background_image = blog_blog_object.background_image
                except:
                    background_image = ''
        # except:
        #     return request.redirect("/aditim/blog/create_new_post_title")

        month = datetime.date.today().month
        year = datetime.date.today().year
        filter_value = '%s_%s' % (month, year)

        time_filter_pool = pool.get('blog.time.filter')
        time_filter_ids = time_filter_pool.search(cr, uid, [('value', '=', filter_value)], context=context)
        if not time_filter_ids:
            values = {
                'value': str(filter_value),
                'month': str(month),
                'year': str(year),
            }
            time_filter_pool.create(cr, uid, values, context=context)
        value = {
            'blog_id': post.get('blog_blog'),
            'name_rus': post.get('post_title'),
            'subtitle': post.get('post_subtitle'),
            'content': '',
            'website_published': False,
            'time_filter': filter_value,
            'website': 'aditim',
        }
        try:
            value['background_image'] = background_image
            value['background_image_small'] = background_image_small
        except:
            pass
        new_blog_post_id = blog_post_pool.create(cr, uid, value, context=context)
        new_blog_post = request.registry['blog.post'].browse(cr, uid, new_blog_post_id, context=context)
        return werkzeug.utils.redirect(
            "/aditim/blog/%s/blog_post/%s?enable_editor=1" % (slug(new_blog_post.blog_id), slug(new_blog_post)))

    @http.route(['/aditim/blog/post/comment/<model("blog.post"):blog_post_object>'], type='http', auth="public",
                website=True)
    def save_blog_comments(self, blog_post_object=None, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        if not form_data or not form_data.get('g-recaptcha-response'):
            return redirect("/aditim/blog/%s/blog_post/%s" % (slug(blog_post_object.blog_id), slug(blog_post_object)))
        email = form_data.get('mail')
        values = {
            'user_id': uid,
            'blog_post_id': blog_post_object.id,
            'name': form_data.get('name'),
            'email': email,
            'text_comment': form_data.get('blog_comment'),
        }

        blog_comments_pool = pool.get('blog.comment')
        blog_comments_pool.create(cr, uid, values, context=context)

        pool.get('blog.post').write(cr, 1, blog_post_object.id, {
            'comment': blog_post_object.comment + 1,
            'last_time_comment': datetime.datetime.today()
        }, context=context)

        crm_lead_pool = pool.get('crm.lead')
        crm_lead_ids = crm_lead_pool.search(cr, 1, [('email_from', '=', email)], context=context)
        if not crm_lead_ids:
            crm_lead_pool.create(cr, 1, {
                'name': form_data.get('name'),
                'partner_name': form_data.get('name'),
                'email_from': email,
            }, context=context)
        return redirect("/aditim/blog/%s/blog_post/%s" % (slug(blog_post_object.blog_id), slug(blog_post_object)))

    @http.route('/aditim/ask_us', type='http', auth="public", website=True)
    def ask_us(self, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        blog_post_pool = pool.get('blog.post')
        question_ids = blog_post_pool.search(cr, uid, [('ask', '=', True), ('website_published', '=', True),
                                                       ('website', '=', 'aditim')], context=context)
        questions = blog_post_pool.browse(cr, uid, question_ids, context=context)
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)

        values = {
            'res_users_object': res_users_object,
            'questions': questions,
            'aditim': '1'
        }
        return request.website.render("website.aditim_ask_us", values)

    @http.route('/aditim/create_ask', type='http', auth="user", website=True)
    def create_ask(self, **form_data):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        blog_post_pool = pool.get('blog.post')
        question_ids = blog_post_pool.search(cr, uid, [('ask', '=', True), ('website_published', '=', True),
                                                       ('website', '=', 'aditim')], context=context)
        questions = blog_post_pool.browse(cr, uid, question_ids, context=context)
        values = {
            'questions': questions,
            'aditim': '1'
        }
        if not form_data or not form_data.get('g-recaptcha-response'):
            return request.website.render("website.aditim_ask_us", values)

        values = {
            'user_id': uid,
            'title': form_data.get('title'),
            'question': form_data.get('question'),
            'name': form_data.get('name'),
            'email': form_data.get('email'),
            'company': form_data.get('company'),
            'questions': questions,
            'website': 'aditim',
        }
        question_pool = pool.get('question')
        question_id = question_pool.create(cr, uid, values, context=context)

        blog_post_pool = pool.get('blog.post')
        month = dt.date.today().month
        year = dt.date.today().year
        filter_value = '%s_%s' % (month, year)

        time_filter_pool = pool.get('blog.time.filter')
        time_filter_ids = time_filter_pool.search(cr, uid, [('value', '=', filter_value)], context=context)
        if not time_filter_ids:
            values = {
                'value': str(filter_value),
                'month': str(month),
                'year': str(year),
            }
            time_filter_pool.create(cr, uid, values, context=context)
        blog_blog_pool = pool.get('blog.blog')
        blog_blog_id = blog_blog_pool.search(cr, uid, [('ask', '=', True), ('website', '=', 'aditim')], context=context)
        if not blog_blog_id:
            blog_blog_id = blog_blog_pool.create(cr, 1, {
                'name': 'Вы спрашивали',
                'filter_link': 'You_ask',
                'ask': True,
                'website': 'aditim',
            })
        else:
            blog_blog_id = blog_blog_id[0]

        value = {
            'blog_id': blog_blog_id,
            'name_rus': form_data.get('title'),
            'subtitle': form_data.get('question'),
            'content': '',
            'website_published': False,
            'time_filter': filter_value,
            'question_id': question_id,
            'ask': True,
            'website': 'aditim'
        }
        blog_post_pool.create(cr, uid, value, context=context)

        return redirect("/aditim/ask/%s" % question_id)

    @http.route(["/aditim/ask/<model('question'):question>"], type='http', auth="public", website=True)
    def ask(self, question, enable_editor=None, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        blog_post_pool = pool.get('blog.post')
        question_ids = blog_post_pool.search(cr, uid, [('ask', '=', True), ('website_published', '=', True),
                                                       ('website', '=', 'aditim')],
                                             context=context)
        questions = blog_post_pool.browse(cr, uid, question_ids, context=context)
        values = {
            'question': question,
            'questions': questions,
            'aditim': '1'
        }
        return request.website.render("website.aditim_ask_page", values)

    @http.route('/aditim/logout', type='http', auth="none")
    def logout(self):
        request.session.logout(keep_db=True)
        return redirect("/aditim/blog")

    @http.route('/aditim/about_us', type='http', auth="public", website=True)
    def about_us(self):
        return request.website.render("website.aditim_about_us", {'aditim': '1'})

    @http.route('/aditim/policy', type='http', auth="public", website=True)
    def policy(self):
        return request.website.render("website.aditim_policy", {'aditim': '1'})

    @http.route('/aditim/rules', type='http', auth="public", website=True)
    def rules(self):
        return request.website.render("website.aditim_rule", {'aditim': '1'})

    @http.route('/aditim/chg_pass', type='http', auth="user", website=True)
    def change_password(self, **post):
        old_password = post.get('old_password')
        new_password = post.get('new_password')
        confirm_password = post.get('confirm_password')

        if not (old_password.strip() and new_password.strip() and confirm_password.strip()):
            values = {
                'error': 'Не правильно заполненая форма',
                'aditim': '1',
            }
            return request.website.render("website.aditim_profile", values)
        if new_password != confirm_password:
            values = {
                'error': 'Новый пароль и пароль подтверждения разные',
                'aditim': '1',
            }
            return request.website.render("website.aditim_profile", values)
        try:
            if request.session.model('res.users').change_password(old_password, new_password):
                values = {
                    'message': 'Пароль изменено',
                    'aditim': '1',
                }
                return request.website.render("website.aditim_profile", values)
        except Exception:
            values = {
                'error': 'Неправильный старый пароль',
                'aditim': '1',
            }
            return request.website.render("website.aditim_profile", values)

    @http.route('/aditim/search', type='http', auth="public", website=True, method='get')
    def site_search(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        blog_post_pool = pool.get('blog.post')
        search_value = post.get('search')
        if not search_value:
            pass
        else:
            search_value = search_value.lower()
            blog_post_ids = blog_post_pool.search(cr, uid, [('website_published', '=', True),('website', '=', 'aditim')], context=context)
            blog_post_objects = blog_post_pool.browse(cr, uid, blog_post_ids, context=context)
            ids = []
            for blog_post in blog_post_objects:
                if search_value in blog_post.name.lower() and blog_post.id not in ids:
                    ids.append(blog_post.id)
                if search_value in blog_post.subtitle.lower() and blog_post.id not in ids:
                    ids.append(blog_post.id)

            search_value_list = search_value.split(' ')

            for blog_post in blog_post_objects:
                for search_value_l in search_value_list:
                    if search_value_l in blog_post.name.lower() and blog_post.id not in ids:
                        ids.append(blog_post.id)
                    if search_value_l in blog_post.subtitle.lower() and blog_post.id not in ids:
                        ids.append(blog_post.id)
            blog_post_objects = blog_post_pool.browse(cr, uid, ids,
                                                      context=context)
            values = {
                'blog_data': blog_post_objects,
                'aditim': '1',
            }
            return request.website.render('website.aditim_search', values)



class AditimHome(openerp.addons.web.controllers.main.Home):

    # Эта функия используется на странице "Вход" для получения списка способов авторизации, заданных
    # в админке, по ссылке
    # http://energy.aditim.ru/web?debug=#page=0&limit=80&view_type=list&model=auth.oauth.provider&menu_id=149&action=167
    # скрин http://pvoytko.ru/jx/WA9LSMXn2L
    def list_providers(self):

        # тут получаем словарь провайдеров из админки
        try:
            provider_obj = request.registry.get('auth.oauth.provider')
            providers = provider_obj.search_read(request.cr, SUPERUSER_ID, [('enabled', '=', True)])
        except Exception:
            providers = []

        # Перебираем все их и для каждого наша цель - сформировать ссылку
        for provider in providers:

            # УРл на который юзер попадает - для Вконтакте в нем указан ID провайдера.
            # чтобы в его обработчике отличать.
            if provider['id'] == 4:
                return_url = plogic.getVkOAuthRedirectUrl()
            else:
                return_url = request.httprequest.url_root + 'auth_oauth/signin'

            # Общие для всех соц. сетей параметры в урл авторизации
            params = dict(
                client_id=provider['client_id'],
                redirect_uri=return_url,
                scope=provider['scope'],
            )

            # Специфичные параметры для LI
            if provider.get('name') in ('LinkedIN'):
                params['response_type'] = 'code'

            # Специфичные параметры для ВК
            elif provider.get('name') in ('VK'):
                params['display'] = 'page'
                params['v'] = '5.60'
                params['response_type'] = 'code'
                # params['revoke'] = '1'

            # Остальные
            else:
                params['response_type'] = 'token'

            # В переменной state словарь
            #  d - имя базы (энерди или полимеры)
            #  p - айди провайдера
            #  еще что-то (они используются когда из соц. сети обработно в наше приложние поаадем
            #  то они передаются через урл. чтобы наш обработчик знал от какой соц. сети авторизация происходит.
            state = self.get_state(provider)
            state_as_url_str = urllib.quote_plus(json.dumps(state))
            params['state'] = state_as_url_str
            state2 = json.loads(urllib.unquote_plus(params['state']))

            provider['auth_link'] = provider['auth_endpoint'] + '?' + werkzeug.url_encode(params)

            print provider.get('name'), state_as_url_str, provider['auth_link']

        return providers


    def get_website_name(self):
        site_url = request.httprequest.url
        host_url = request.httprequest.host_url
        try:
            website_name = site_url.replace(host_url, '').split('/')[0]
        except:
            website_name = 'aditim'
        return website_name

    @http.route(['/aditim/signin', '/aditim/web/login',
                 '/web/login', '/aditim/web/signin',
                 '/web/signin', '/polymeres/signin',
                 '/polymeres/web/login', '/polymeres/web/signin'], type='http', auth="none")
    def web_login(self, redirect=None, **kw):

        ensure_db()
        if request.httprequest.method == 'GET' and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)
        if not request.uid:
            request.uid = openerp.SUPERUSER_ID
        values = request.params.copy()
        website_name = self.get_website_name()
        if not redirect:
            redirect = '/' + website_name + '/web?' + \
                       request.httprequest.query_string
        values['redirect'] = redirect
        try:
            values['databases'] = http.db_list()
        except openerp.exceptions.AccessDenied:
            values['databases'] = None
        if request.httprequest.method == 'POST':
            old_uid = request.uid
            uid = request.session.authenticate(request.session.db,
                                               request.params['login'],
                                               request.params['password'])
            if uid is not False and kw.get('g-recaptcha-response'):
                if website_name == 'aditim':
                    return request.redirect('/aditim/lk')
                elif website_name == 'polymeres':
                    return request.redirect('/polymeres/lk')
            request.uid = old_uid
            values['error'] = _("Вы ввели неправильно имя или пароль")
        if website_name == 'polymeres':
            values['polymeres'] = '1'
        else:
            values['aditim'] = '1'
        values['providers'] = self.list_providers()
        if request.env.ref('website.aditim_login', False):
            return request.render('website.aditim_login', values)
        else:
            # probably not an odoo compatible database
            return werkzeug.utils.redirect('/web/database/selector')

    # Стрница входа - возвращем список ссылок для авторизации через соц. сети.
    @http.route(['/aditim/registration', '/aditim/web/signup',
                 '/polymeres/registration', '/polymeres/web/signup'],
                type='http', auth='public', website=True)
    def web_page_auth_signup(self, *args, **kw):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        website_name = self.get_website_name()
        qcontext = self.get_auth_signup_qcontext()
        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()
        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                self._send_verification_mail()
                if website_name != 'polymeres':
                    return request.redirect('/aditim/signin')
                else:
                    return request.redirect('/polymeres/signin')

            except (SignupError, AssertionError), e:
                qcontext['error'] = _(e.message)

        values = {
            'qcontext': qcontext,
            'user_login': True,
            'providers': self.list_providers(),
        }
        if  website_name == 'polymeres':
            values['polymeres'] = '1'
        else:
            values['aditim'] = '1'
        return request.render('website.aditim_registration', values)

    def get_auth_signup_config(self):
        """retrieve the module config (which features are enabled) for the login page"""

        icp = request.registry.get('ir.config_parameter')
        return {
            'signup_enabled': icp.get_param(request.cr, openerp.SUPERUSER_ID, 'auth_signup.allow_uninvited') == 'True',
            'reset_password_enabled': icp.get_param(request.cr, openerp.SUPERUSER_ID,
                                                    'auth_signup.reset_password') == 'True',
        }

    def get_auth_signup_qcontext(self):
        """ Shared helper returning the rendering context for signup and reset password """
        qcontext = request.params.copy()
        qcontext.update(self.get_auth_signup_config())
        if qcontext.get('token'):
            try:
                # retrieve the user info (name, login or email) corresponding to a signup token
                res_partner = request.registry.get('res.partner')
                token_infos = res_partner.signup_retrieve_info(request.cr, openerp.SUPERUSER_ID, qcontext.get('token'))
                for k, v in token_infos.items():
                    qcontext.setdefault(k, v)
            except:
                qcontext['error'] = _("Invalid signup token")
        return qcontext

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        values = dict((key, qcontext.get(key)) for key in ('login', 'name', 'password', 'last_name'))
        assert any([k for k in values.values()]), "The form was not properly filled in."
        # assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()

    def _signup_with_values(self, token, values):
        db, login, password = request.registry['res.users'].signup(request.cr, openerp.SUPERUSER_ID, values, token)
        request.cr.commit()  # as authenticate will use its own cursor we need to commit the current transaction
        # uid = request.session.authenticate(db, login, password)
        # if not uid:
        #     raise SignupError(_('Authentification Failed.'))

    def _send_verification_mail(self):
        print "\n\n\n\n"
        qcontext = self.get_auth_signup_qcontext()
        login = qcontext.get('login')
        res_users = request.registry.get('res.users')
        try:
            res_users.user_varification_mail(request.cr, openerp.SUPERUSER_ID, login)
            qcontext['message'] = _(
                "An email has been sent with credentials to reset your password")
        except:
            qcontext['message'] = _("Server error")
        print "this is the qcontext=====>", qcontext

    @http.route(['/registration/checkemail'], type='json', auth="public", methods=['POST'], website=True)
    def check_user_email(self, **kw):
        email = kw.get('email')
        cr, context, pool = request.cr, request.context, request.registry
        res_partner = pool.get('res.partner')
        res_users = pool.get('res.users')
        email_ids = res_partner.search(cr, SUPERUSER_ID, [('email', '=', email)], context=context)
        login_ids = res_users.search(cr, SUPERUSER_ID, [('login', '=', email)], context=context)
        if login_ids:
            return False
        if email_ids:
            return False
        return True

    @http.route(['/aditim/reset_password', '/polymeres/reset_password'], type='http', auth='public', website=True)
    def web_auth_reset_password(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()
        website_name = self.get_website_name()
        if  website_name == 'polymeres':
            qcontext['polymeres'] = '1'
        else:
            qcontext['aditim'] = '1'
        if not qcontext.get('token') and not qcontext.get('reset_password_enabled'):
            raise werkzeug.exceptions.NotFound()
        if 'error' not in qcontext and request.httprequest.method == 'POST':

            try:
                if qcontext.get('token'):
                    self.do_signup(qcontext)
                    return super(AditimHome, self).web_login(*args, **kw)
                else:
                    login = qcontext.get('login')
                    assert login, "Не правильный пароль"
                    res_users = request.registry.get('res.users')
                    res_users.reset_password(request.cr, openerp.SUPERUSER_ID, login)
                    qcontext['message'] = _("Вам на почту была отправлена инструкция по востановлению пароля")
            except SignupError:
                qcontext['error'] = _("Нельзя востановить пароль")
                _logger.exception('Ошибка при востановлении пароля')
            except Exception, e:
                qcontext['error'] = e.message
        return request.render('website.aditim_reset_password', qcontext)

def fragment_to_query_string(func):
    @functools.wraps(func)
    def wrapper(self, *a, **kw):
        if not kw:
            return """<html><head><script>
                var l = window.location;
                var q = l.hash.substring(1);
                var r = l.pathname + l.search;
                if(q.length !== 0) {
                    var s = l.search ? (l.search === '?' ? '' : '&') : '?';
                    r = l.pathname + l.search + s + q;
                }
                if (r == l.pathname) {
                    r = '/';
                }
                window.location = r;
            </script></head><body></body></html>"""
        return func(self, *a, **kw)
    return wrapper



# На эту страницу юзер попадает после нжатия на "Подтвердить" или "Отмена" при:
# * авторизации через соц. сети.
# * регистрации через соц. сети.
class OAuthController_inherit(OAController):
    @http.route('/auth_oauth/signin', type='http', auth='none')
    @fragment_to_query_string
    def signin(self, **kw):

        # возвращает True если переданный словарь содержит этот ключ и это значение
        # и False вп ротивном случае.
        def hasKeyWithValue(dict_for_test, key_name, key_value):
            if key_name in dict_for_test and dict_for_test[key_name] == key_value:
                return True
            return False

        # В kw содержится параметры из соц. сети.
        # state обязательное, содержится в ссылке для авторизации в соц. сети.
        # содержит имя базы, айди провайдера.
        # То что передалось в адрес при клике.
        # почему-то для ВК он передается иначе чем для других соц. сетей.
        if 'AD_AUTH2_VK' in kw:
            state = json.loads(kw['state'])
            pass
        else:
            state = json.loads(urllib.unquote_plus(kw['state']))

        # Этот обработчик использутеся для всех соц. сетей.
        # Он уже был сделан для гугл плюс и линкедин, глючный код ниже (в ветке else)
        # а для вконтакте я написал свой. Поэтому тут идет выбор, использовать старый код или новый.
        # Новй надо если в kw есть state и имеет занчение вконтакте 4
        if 'AD_AUTH2_VK' in kw:

            # Тут возможно два варианта. 1. Юзер запретил вход.
            # Тогда делаем возврат на исхоную страницу. Критерий - в поле error содержится 'access_denied'
            if hasKeyWithValue(kw, 'error', u'access_denied'):
                return set_cookie_and_redirect('/aditim/signin')

            # Доступ разрешен
            # тут наша задача получить аккаунт юзера на сайте,
            # соответствующий его аккаунту в соц. сети.
            # (а если такого нет то показать ошибку) и пройти регистрацию сперва.
            elif 'code' in kw:

                # имя базы (два сайта на эту страницу - энерджи и полимеры) поступает в параметре d
                # и id провадйера.
                #
                # Далее - укрупненно: имея код, получаем access_token. а имя access_token,
                # получаем емейл (он же логин на сайте) и фио из вконтакте и если его нет в БД то регистриуем.
                # И затем авторизуем его (Устанавливаем куку).

                # Теперь деталеьно.

                # Шаг 1 - Получить фио и емейл из Вконтакте.
                vk_token, user_vk_id, user_vk_email = plogic.getVkAccessTokenByCode(kw['code'])
                fio = plogic.getVkFioByAcessToken(vk_token)

                # Шаг 2 - если акк есть, то получаем его из бд.
                res_users_registry = request.registry('res.users')
                acc_existing = plogic.getUserFromDbByEmail(request, user_vk_email)

                # Шаг 3. Если акка нет, то создаем (регистрируем).
                if acc_existing is None:
                    user_fields = {}
                    user_fields['email'] = user_vk_email
                    user_fields['login'] = user_vk_email
                    user_fields['password'] = user_vk_email
                    user_fields['name'] = fio
                    user_fields['oauth_provider_id'] = 4 # это чтоб активным стало. Код создания юзера проверяет флаг.
                    udbn, ul, up = request.registry['res.users'].signup(request.cr, openerp.SUPERUSER_ID, user_fields, None)
                    acc_existing = plogic.getUserFromDbByEmail(request, user_vk_email)

                    # Без этого на момент авторизации юзера еще нет в БД
                    request.cr.commit()

                # Шаг 4. Авторизуем.
                dbname = state['d']
                return login_and_redirect(dbname, user_vk_email, user_vk_email, redirect_url='/aditim/lk')
                # provider = state['p']
                # registry = RegistryManager.get(dbname)
                # with registry.cursor() as cr:
                #     u = registry.get('res.users')
                #     kw = json.loads('''{"access_token": "ya29.CjCvA2SG61BWVSKygIgi_S0BL5N5GpcgfmmPmdWMphMEzDRazjqp4VxNHSLpOrkxlMQ", "token_type": "Bearer", "state": "%7B%22p%22%3A+3%2C+%22r%22%3A+%22http%253A%252F%252Fenergy.aditim.ru%252Fweb%22%2C+%22d%22%3A+%22aditim%22%7D", "expires_in": "3600"}''')
                #     credentials = u.auth_oauth(cr, SUPERUSER_ID, 4, kw, context=None)
                #     cr.commit()
                #     return login_and_redirect(*credentials, redirect_url='/aditim/lk')

                return set_cookie_and_redirect('/')

            # Не знаю о возможности других исходов, поэому тут проверка на всякий случай.
            else:
                raise RuntimeError(
                    u"""Какой-то другой исход авторизации в соц. сети кроме предусмотренных двух,
                    либо вызвали урл с непредусмотренными параметрами."""
                )

        # См. комент к ветке if. Это обработчик гугл плюс, линкед ин и т.п.
        else:
            cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
            site_url = request.httprequest.url_root
            print "=====Value of KW is ====>>>>===KWWW", kw

            # Linked IN API return first code so using code we get access token...
            if "code" in kw.keys():
                if (kw['code'] != ""):
                    # Code for LinkedIn Authentication for local server.

                    API_KEY = "77z6iq3i2xzu0i"
                    API_SECRET = "l5DP4yVPoJLDhobf"

                    RETURN_URL = site_url + "auth_oauth/signin"
                    authentication = linkedin.LinkedInAuthentication(API_KEY,
                                                                     API_SECRET,
                                                                     RETURN_URL,
                                                                     linkedin.PERMISSIONS.enums.values())
                    application = linkedin.LinkedInApplication(authentication)
                    authentication.authorization_code = kw['code']
                    token = authentication.get_access_token()
                    kw['access_token'] = token.access_token
                    # print "\n\n\nAccess Token is =======>",token.access_token
            print "Actual value of State is ===> ", state

            dbname = state['d']
            provider = state['p']
            registry = RegistryManager.get(dbname)
            with registry.cursor() as cr:
                try:
                    u = registry.get('res.users')
                    credentials = u.auth_oauth(cr, SUPERUSER_ID, provider, kw, context=None)
                    cr.commit()
                    action = state.get('a')
                    menu = state.get('m')
                    redirect = werkzeug.url_unquote_plus(state['r']) if state.get('r') else False
                    url = '/aditim/lk'
                    if redirect:
                        url = redirect
                    elif action:
                        url = '/web#action=%s' % action
                    elif menu:
                        url = '/web#menu_id=%s' % menu
                    url = url.replace('/web', '/aditim/lk')
                    return login_and_redirect(*credentials, redirect_url=url)
                except AttributeError:
                    # auth_signup is not installed
                    print "\n\n\n Error while login.........."
                    _logger.error(
                        "auth_signup not installed on database %s: oauth sign up cancelled." % (
                        dbname,))
                    url = "/web/login?oauth_error=1"
                except openerp.exceptions.AccessDenied:
                    # oauth credentials not valid, user could be on a temporary session
                    _logger.info(
                        'OAuth2: access denied, redirect to main page in case a valid session exists, without setting cookies')
                    url = "/web/login?oauth_error=3"
                    redirect = werkzeug.utils.redirect(url, 303)
                    redirect.autocorrect_location_header = False
                    return redirect
                except Exception, e:
                    # signup error
                    _logger.exception("OAuth2: %s" % str(e))
                    url = "/web/login?oauth_error=2"

            return set_cookie_and_redirect(url)
        # vim:expandtab:tabstop=4:softtabstop=4:shiftwidth=4: