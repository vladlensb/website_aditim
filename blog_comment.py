# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields


class BlogComment(osv.osv):
    _name = 'blog.comment'
    _description = 'Blog Coment'

    _columns = {
        'user_id': fields.many2one('res.users', 'User'),
        'blog_post_id': fields.many2one('blog.post', 'Blog Post'),
        'name': fields.char('Name'),
        'email': fields.text('Description'),
        'text_comment': fields.char('Filter Link', required=True),

    }


BlogComment()
