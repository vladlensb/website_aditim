# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api
import logging

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    company_name = fields.Char(string='Company Name',
                               translate=True)
    last_name = fields.Char(string='Last Name',
                            translate=True)
    first_name = fields.Char(string='Last Name',
                             translate=True)
    email_private = fields.Char(string="Private Email")
    email_other = fields.Char(string="Other Email")

    @api.multi
    def add_contact(self):
        files = ['companies.csv', 'contacts.csv']

        for file_name in files:
            f = open('/home/odoo/website_aditim/data/%s' % file_name)
            # f = open('/home/rokealva/Production/aditim/odoo-9.0/openerp/addons/'
            #          'website_aditim/data/%s' % file_name)
            lines = f.readlines()
            i = len(lines)
            for line in lines:
                _logger.info('Number %s', i)
                line_value = line.split(';')
                values = {'active': True, }
                if file_name == 'companies.csv':
                    values['name'] = line_value[0]
                    if line_value[1] == '':
                        values['customer'] = True
                    elif line_value[1] == '':
                        values['supplier'] = True
                    values['email'] = line_value[2]
                    values['email_private'] = line_value[3]
                    values['email_other'] = line_value[4]
                    values['website'] = line_value[5]

                    values['street'] = line_value[6]
                    values['is_company'] = True
                if file_name == 'contacts.csv':
                    values['type'] = 'contact'

                    name = line_value[0]
                    if line_value[1]:
                        name = '%s %s' % (name, line_value[1])
                    if line_value[2]:
                        name = '%s %s' % (name, line_value[2])
                    values['name'] = name
                    values['email'] = line_value[3]
                    values['email_private'] = line_value[4]
                    values['email_other'] = line_value[5]
                    companies = self.search([('is_company', '=', True),
                                             ('name', '=', line_value[6])])
                    if companies and len(companies) == 1:
                        values['parent_id'] = companies.id
                if line_value[0]:
                    self.create(values)

                i -= 1
