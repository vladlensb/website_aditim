# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields
import base64


class blog_slider(osv.Model):
    _name = "blog.slider"

    _columns = {
        'image': fields.binary('Image'),
        'title': fields.char('Title'),
        'subtitle': fields.char('Subtitle'),
        'button': fields.char('Button Text'),
        'button_link': fields.char('Button Link'),
        'show': fields.boolean('Show'),
        'id_str': fields.char('str'),
        'website': fields.selection([('aditim', 'Aditim'),
                                     ('polymeres', 'Polymeres'),
                                     ], 'Website'),
    }

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('image'):
            imgdata = base64.b64decode(vals.get('image'))
            # f = open('addons/website_aditim/static/images/blog_slider/%s.jpg' % ids[0], 'wb')
            f = open('/home/odoo/website_aditim/static/images/blog_slider/%s.jpg' % ids[0], 'wb')
            f.write(imgdata)
            f.close()
        return super(blog_slider, self).write(cr, uid, ids, vals, context=context)

    def create(self, cr, uid, vals, context=None):
        blog_slider_id = super(blog_slider, self).create(cr, uid, vals, context)
        image = self.browse(cr, uid, blog_slider_id, context=context).image
        imgdata = base64.b64decode(image)
        # f = open('addons/website_aditim/static/images/blog_slider/%s.jpg' % blog_slider_id, 'wb')
        f = open('/home/odoo/website_aditim/static/images/blog_slider/%s.jpg' % blog_slider_id, 'wb')
        f.write(imgdata)
        f.close()
        s = '/website_aditim/static/images/blog_slider/%s.jpg' % blog_slider_id
        self.write(cr, uid, blog_slider_id, {'id_str': s})
        return blog_slider_id

blog_slider()
