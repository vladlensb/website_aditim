/**
 * Created by tadej on 12.06.16.
 */
// **************    count simbol


$(document).ready(function () {
    $("#article-title").keyup(function () {
        var box = $(this).val();
        var count = 200 - box.length;

        if (box.length <= 200) {
            $('#title_count').html(count);
        }
        return false;
    });
});

$(document).ready(function () {
    $("#article-short-precis").keyup(function () {
        var box = $(this).val();
        var count = 500 - box.length;

        if (box.length <= 500) {
            $('#subtitle_count').html(count);
        }
        return false;
    });
});


$(document).ready(function () {
    $('#popup_red_button').click(function () {
        $("#popup_bg").css('display', 'none');
        $("#popup_registration").css('display', 'none');
    });
});

$(document).ready(function () {
    $('#registration_button').click(function () {
        var login = $('#login').val();
        var company = $('#company').val();
        var password = $('#password').val();
        var name = $('#name').val();

        if (login != '' && company  != '' &&  password != '' && name != ''){
            $("#popup_bg").css('display', 'block');
            $("#popup_registration").css('display', 'block');
        }



    });
});

$(document).ready(function () {
    $('#save_account').click(function () {
        $("#popup_bg").css('display', 'block');
        $("#popup_lk").css('display', 'block');
    });
});

$(document).ready(function () {
    $('#save_chg_pass').click(function () {
        $("#popup_bg").css('display', 'block');
        $("#popup_lk").css('display', 'block');
    });
});

$(document).ready(function () {
    $('#checkbox').on('change', function () {
        if ($(this).prop("checked")) {
            $('#social_links').find('.provider').removeClass('not-active');
            $('#oe_login_buttons').removeAttr('disabled');
            $('#registration_button').removeAttr('disabled')
            $('#ask_for_us').removeAttr('disabled')
            $('#reset_pass').removeAttr('disabled')

        }
        else {
            $('#social_links').find('.provider').addClass('not-active');
            $('#oe_login_buttons').attr('disabled', 'disabled');
            $('#registration_button').attr('disabled', 'disabled')
            $('#ask_for_us').attr('disabled', 'disabled')
            $('#reset_pass').attr('disabled', 'disabled')
        }
    })
});