/**
 * @file
 * Custom scripts for theme.
 */
odoo.define('web.Session', function (require) {
    "use strict";

    var ajax = require('web.ajax');
    
    (function ($) {
        // Fn to allow an event to fire after all images are loaded
        $.fn.imagesLoaded = function () {

            // get all the images (excluding those with no src attribute)
            var $imgs = this.find('img[src!=""]');
            // if there's no images, just return an already resolved promise
            if (!$imgs.length) {
                return $.Deferred().resolve().promise();
            }

            // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
            var dfds = [];
            $imgs.each(function () {

                var dfd = $.Deferred();
                dfds.push(dfd);
                var img = new Image();
                img.onload = function () {
                    dfd.resolve();
                }
                img.onerror = function () {
                    dfd.resolve();
                }
                img.src = this.src;

            });

            // return a master promise object which will resolve when all the deferred objects have resolved
            // IE - when all the images are loaded
            return $.when.apply($, dfds);

        }

        function rebuild_title_blog() {
            $('.front_blog_img').css('min-height', $('.front_blog_img h1').height());
        }

        $(window).load(function () {
            $('.main_blog .list').masonry({itemSelector: '.item'});

            $('.upload_more a').on('click', function () {
                // create new item elements
                var count = $('.main_blog .list .item').length;

                /*$.post("ajax_blog.php",
                 {
                 count:count,
                 time_filter_name:$.trim($('#time_filter_name').html()),
                 filter_name:$.trim($('#filter_name').html())
                 },
                 function(data){
                 var $items = $(data);
                 // append items to grid
                 $( '.main_blog .list' ).append( $items ).imagesLoaded().then(function(){
                 $( '.main_blog .list' ).masonry( 'appended', $items );
                 });
                 });*/
                ajax.jsonRpc('/polymeres/blog/upload_more', 'call', {
                    count: count,
                    time_filter_name: $.trim($('#time_filter_name').html()),
                    filter_name: $.trim($('#filter_name').html())
                }).then(function (data) {
                    if (data['have_more'] == 0) {
                        $('.upload_more a').css('display', 'none');
                    }
                    var $items = $(data['data']);
                    // append items to grid
                    $('.main_blog .list').append($items).imagesLoaded().then(function () {
                        $('.main_blog .list').masonry('appended', $items);
                    });
                    // add and lay out newly appended items

                });
                return false;
            });
            rebuild_title_blog();
        });
        $(window).resize(function () {
            rebuild_title_blog();
        });
        $(document).ready(function () {
            $("#hide").click(function () {
                $(".navbar-brand").removeClass("hidden-xs");
                $(".search-box").css("display", "none");
                $("#hide").css("display", "none");
                $("#hide_search").css("display", "none");
                $("#show").css("display", "block");

            });
            $("#show").click(function () {
                $(".navbar-brand").addClass("hidden-xs");
                $(".search-box").css("display", "block");
                $("#hide").css("display", "block");
                $("#hide_search").css("display", "block");
            });
            $('#myCarousel').find('.item:first-child').addClass('active');
            $('.carousel-indicators').find('li').addClass('active');
        });

        $(document).ready(function () {
            $(".search-btn").click(function () {
                $(this).hide();
            });
        });
        $(document).ready(function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > 0) {
                $('#wrapwrap').addClass('scrolled');
            } else {
                $('#wrapwrap').removeClass('scrolled');
            }
        });
        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > 0) {
                $('#wrapwrap').addClass('scrolled');
            } else {
                $('#wrapwrap').removeClass('scrolled');
            }
        });
        $(document).ready(function () {
            if ($('#owl-answers').length > 0) {

                var owl = $("#owl-answers");

                owl.owlCarousel({

                    itemsCustom: [
                        [0, 1],
                        [640, 2],
                        [800, 3],
                    ],

                    pagination: false

                });
                $(".owl-control .next").click(function () {
                    owl.trigger('owl.next');
                })
                $(".owl-control .prev").click(function () {
                    owl.trigger('owl.prev');
                })
            }


        });

        $('#checkbox').on('change', function () {
            console.log('TEST')
            var social_links = document.getElementById("social_links");
            console.log(social_links)
            if ($(this).prop("checked")){

                social_links.find('a').removeClass('not-active');
            }
            else{
                social_links.find('a').addClass('not-active');
            }
        })

    })(jQuery);
})

