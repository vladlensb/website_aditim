# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields


class Blog(osv.Model):
    _name = 'blog.blog'
    _description = 'Blogs'
    _inherit = 'blog.blog'

    def _get_link(self, cr, uid, ids, name, arg, context=None):
        res = {}
        blog_blog_pool = self.pool.get('blog.blog')
        blog_blog_objects = blog_blog_pool.browse(cr, uid, ids, context=context)
        link = blog_blog_objects.name.replace(' ', '_').lower()
        res[ids[0]] = link
        return res

    _columns = {
        'name': fields.char('Blog Name', required=True, translate=True),
        'subtitle': fields.char('Blog Subtitle', translate=True),
        'description': fields.text('Description', translate=True),
        'filter_link': fields.char('Filter Link', required=True),
        'other_prev': fields.boolean('Other Prev'),
        'ask': fields.boolean('Ask Category'),
        'website':fields.selection([('aditim', 'Aditim'),
                                       ('polymeres', 'Polymeres'),
                                       ], 'Website'),
    }


Blog()
