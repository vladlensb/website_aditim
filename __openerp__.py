# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'ADITIM website',
    'version': '1.0',
    'author': 'Libre Comunication',
    'category': 'ADITIM',

    'description': """
        Module for processing drawings
        """,

    'depends': [
        'website_blog',
    ],

    'data': [
        'security/ir.model.access.csv',
        'security/group_security.xml',

        'views/assets.xml',
        'views/aditim_layout.xml',
        # 'views/polymeres_layout.xml',
        'views/aditim_mail_verification_template.xml',
        'views/snippets.xml',


        'web/main_page.xml',
        'web/login_page.xml',
        'web/registration_page.xml',
        'web/lk_page.xml',
        'web/create_blog_post_page.xml',
        'web/blog_post_page.xml',
        'web/reset_pass_page.xml',
        'web/ask_for_us.xml',
        'web/ask_page.xml',
        'web/about_us.xml',
        'web/policy.xml',
        'web/rule.xml',
        'web/search_page.xml',

        'blog_slider_views.xml',
        'question_views.xml',
        'blog_blog_view.xml',
        'blog_post_view.xml',
        # 'res_users_view.xml',
        'res_partner_views.xml',
    ],

    'images': ['static/description/icon.png'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
